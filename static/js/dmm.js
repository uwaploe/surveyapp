// Represent angles as degrees and decimal minutes.
define(function () {
    function Dmm(deg) {
        if(deg == null) {
            this.deg = 0;
            this.min = 0.;
        } else {
            if(deg < 0)
                this.deg = Math.ceil(deg);
            else
                this.deg = Math.floor(deg);

            this.min = (deg - this.deg)*60.;
        }
    }

    Dmm.prototype = {
        tostring: function() {
            var deg = Number(this.deg);
            var min = Number(Math.abs(this.min));
            var padding = (min < 10.) ? "0" : "";
            return deg.toFixed(0) + "-" + padding + min.toFixed(4);
        },
        todegrees: function() {
            return this.deg + this.min/60.;
        },
        isneg: function() {
            return (this.deg < 0);
        },
        abs: function() {
            var d = new Dmm(0);
            d.deg = Math.abs(this.deg);
            d.min = Math.abs(this.min);
            return d;
        }
    };

    return Dmm;
});
