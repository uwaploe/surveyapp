DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

GOBUILD := CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
APP := surveyapp

all: $(APP)

dep:
	@go get -v -d ./...

$(APP): dep
	@$(GOBUILD) -v -o $@ \
	  -tags release \
	  -ldflags '-s -X main.Version=$(VERSION) -X main.BuildDate=$(DATE)'

image: $(APP)
	docker build -t gosurvey:$(VERSION) -t gosurvey:latest .

clean:
	@rm -f $(APP)
