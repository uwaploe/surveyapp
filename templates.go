package main

import (
	"fmt"
	"text/template"
	"time"

	"bitbucket.org/uwaploe/gics/api"
)

// Format grid point coordinates for the survey file template
func gridScale(val int32) string {
	return fmt.Sprintf("%.3f", float64(val)/api.GRIDSCALE)
}

func todm(d float64) (int, float64) {
	deg := int(d)
	min := (d - float64(deg)) * 60
	return deg, min
}

// Format latitude values for a template
func formatLatitude(val int32) string {
	d, m := todm(float64(val) / api.GEOSCALE)
	var hemi rune

	if d < 0 || m < 0 {
		hemi = 'S'
		d = -d
		m = -m
	} else {
		hemi = 'N'
	}

	return fmt.Sprintf("%d-%07.4f%c", d, m, hemi)
}

// Format longitude values for a template
func formatLongitude(val int32) string {
	d, m := todm(float64(val) / api.GEOSCALE)
	var hemi rune

	if d < 0 || m < 0 {
		hemi = 'W'
		d = -d
		m = -m
	} else {
		hemi = 'E'
	}

	return fmt.Sprintf("%d-%07.4f%c", d, m, hemi)
}

func formatTime(tsec int64) string {
	t := time.Unix(tsec, 0).UTC()
	return t.Format("2006-01-02 15:04:05Z")
}

func loadTemplates(cfg *sysConfig) *template.Template {
	funcMap := template.FuncMap{
		"formatLatitude":  formatLatitude,
		"formatLongitude": formatLongitude,
		"formatTime":      formatTime,
		"gridscale":       gridScale,
	}
	glob := cfg.Paths.Templates + "/*"
	tmpl := template.Must(template.New("app").Funcs(funcMap).ParseGlob(glob))
	return tmpl
}
