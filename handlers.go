package main

import (
	"context"
	"log"
	"net/http"

	"bitbucket.org/uwaploe/gics/api"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
)

func getSurvey(conn redis.Conn) *api.Survey {
	val, err := redis.String(conn.Do("LINDEX", "survey", -1))
	if err != nil || val == "" {
		return nil
	}
	s := api.Survey{}
	err = jsonpb.UnmarshalString(val, &s)
	if err != nil {
		log.Printf("JSON unmarshal: %v", err)
		return nil
	}

	return &s
}

// Use the most recent Range Matrix to perform a survey and store the results
// in the Redis data store.
func doSurvey(conn redis.Conn, client api.SurveyorClient,
	rw http.ResponseWriter, req *http.Request) {
	var noarg api.Empty
	xy, err := client.SetGrid(context.Background(), &noarg)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rm, err := client.GetRanges(context.Background(), &noarg)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	ma := jsonpb.Marshaler{EmitDefaults: true}
	xy_msg, err := ma.MarshalToString(xy)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rm_msg, err := ma.MarshalToString(rm)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	conn.Send("MULTI")
	conn.Send("RPUSH", "survey", xy_msg)
	conn.Send("RPUSH", "ranges", rm_msg)
	_, err = conn.Do("EXEC")
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write([]byte(xy_msg))
}
