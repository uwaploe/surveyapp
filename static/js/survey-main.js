// Module loader
require.config({
    paths: {
        baseUrl: "static/js",
        hchart: [
            "lib/highcharts",
            "lib/highcharts-more",
            "lib/exporting"
        ],
        jquery: "lib/jquery.min",
        moment: "lib/moment.min",
        geopoint: "./geopoint",
        parsedmm: "./parseDmm",
        dmm: "./dmm",
        app: "./survey-app",
        plot: "./plotting"
    },
    config: {
        moment: {
            noGlobal: true
        }
    }
});
