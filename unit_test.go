package main

import (
	"log"
	"os"
	"text/template"

	"bitbucket.org/uwaploe/gics/api"
	"github.com/golang/protobuf/jsonpb"
)

var DATA = "{\"tsec\": \"1510077878\", \"points\": {\"h1\": {\"x\": 177993, \"y\": 203226, \"id\": \"h1\"}, \"h2\": {\"x\": -404223, \"y\": 290153, \"id\": \"h2\"}, \"h3\": {\"x\": -383440, \"y\": -432117, \"id\": \"h3\"}, \"h4\": {\"x\": 418132, \"y\": -308844, \"id\": \"h4\"}}}"

func ExampleTemplate() {
	funcMap := template.FuncMap{
		"gridscale": gridScale,
	}

	var err error

	tmpl := template.New("test").Funcs(funcMap)
	tmpl, err = tmpl.ParseFiles("templates/survey.xyz")
	if err != nil {
		log.Fatal(err)
	}

	rec := api.Survey{}
	err = jsonpb.UnmarshalString(DATA, &rec)
	if err != nil {
		log.Fatal(err)
	}

	tmpl.ExecuteTemplate(os.Stdout, "survey.xyz", rec.Points)

	// Output:
	// 1, H1, 177.993, 203.226, 100, Tracking Hyd
	// 2, H2, -404.223, 290.153, 100, Tracking Hyd
	// 3, H3, -383.440, -432.117, 100, Tracking Hyd
	// 4, H4, 418.132, -308.844, 100, Tracking Hyd
	// 5, H5, 177.993, 203.226, 100, Tracking Hyd
	// 6, H6, -404.223, 290.153, 100, Tracking Hyd
	// 7, H7, -383.440, -432.117, 100, Tracking Hyd
	// 8, H8, 418.132, -308.844, 100, Tracking Hyd
}
