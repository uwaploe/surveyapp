FROM alpine:latest

RUN mkdir -p /app/static /app/templates
WORKDIR /app
COPY static /app/static/
COPY templates /app/templates/
COPY surveyapp /app/surveyapp

EXPOSE 80

ENTRYPOINT ["./surveyapp"]
