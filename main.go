// Surveyapp provides a web application to perform the initial survey
// of the ICEX tracking range
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/gics/api"
	"bitbucket.org/uwaploe/go-icex/pkg/sse"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/jsonpb"
	"github.com/gorilla/mux"
	"github.com/imdario/mergo"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type sysConfig struct {
	Server   serviceConfig `toml:"server"`
	Redis    serviceConfig `toml:"redis"`
	Surveyor serviceConfig `toml:"surveyor"`
	Paths    pathsConfig   `toml:"paths"`
}

type serviceConfig struct {
	Address string `toml:"address"`
}

type pathsConfig struct {
	Static    string `toml:"static"`
	Templates string `toml:"templates"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: surveyapp [options]

Web application to perform the initial survey of the ICEX tracking range
`

var DEFAULT = `
[server]
address = ":8081"
[redis]
address = "localhost:6379"
[surveyor]
address = "localhost:10001"
[paths]
static = "./static"
templates = "./templates"
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	debug = flag.Bool("debug", false,
		"Output diagnostic information while running")
	config = flag.String("config", "", "Configuration file")
)

// Coordinates of the lower-triangle of the Range Matrix
var LOWER_TRI = [][]int{
	{1, 0},
	{2, 0}, {2, 1},
	{3, 0}, {3, 1}, {3, 2},
	{4, 0}, {4, 1}, {4, 2}, {4, 3},
}

func reformatRanges(rm *api.RangeMatrix) map[string]float64 {
	if rm.N < 5 {
		return nil
	}

	ranges := make(map[string]float64)
	for _, pair := range LOWER_TRI {
		name := fmt.Sprintf("%s-%s", rm.Ids[pair[0]], rm.Ids[pair[1]])
		r := float64(rm.Ranges[pair[0]*int(rm.N)+pair[1]]) / api.GRIDSCALE * api.YDS_PER_METER
		ranges[name] = r
	}

	return ranges
}

func dataReader(psc redis.PubSubConn, client api.SurveyorClient, b *sse.Broker) {
	var noarg api.Empty
	log.Println("Waiting for messages")
	ma := jsonpb.Marshaler{EmitDefaults: true}
	for {
		switch msg := psc.Receive().(type) {
		case error:
			log.Println(msg)
			return
		case redis.Subscription:
			if msg.Count == 0 {
				log.Println("Pubsub channel closed")
				return
			} else {
				log.Printf("Subscribed to %q", msg.Channel)
			}
		case redis.Message:
			switch msg.Channel {
			case "data.ranges":
				rm := api.RangeMatrix{}
				if err := jsonpb.UnmarshalString(string(msg.Data), &rm); err == nil {
					r := reformatRanges(&rm)
					// Send raw ranges
					if msg, err := json.Marshal(r); err == nil {
						b.Notifier <- sse.Event{Name: []byte("ranges"), Data: msg}
					}
					// Send calculated grid X-Y coordinates
					if xy, err := client.SetGrid(context.Background(), &noarg); err == nil {
						if msg, err := ma.MarshalToString(xy); err == nil {
							b.Notifier <- sse.Event{Name: []byte("refs"), Data: []byte(msg)}
						}
					} else {
						log.Printf("SetGrid error: %v", err)
					}
				} else {
					log.Printf("data.ranges error: %v", err)
				}
			case "data.refs":
				// Extract the origin coordinates from the PointSet
				pset := api.PointSet{}
				if err := jsonpb.UnmarshalString(string(msg.Data), &pset); err == nil {
					if p, ok := pset.Points["origin"]; ok {
						if msg, err := ma.MarshalToString(p); err == nil {
							b.Notifier <- sse.Event{Name: []byte("gps"), Data: []byte(msg)}
						}
					}
				} else {
					log.Printf("data.refs error: %v", err)
				}
			}
		}
	}
}

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func loadConfiguration(base, cfgfile string, cfg *sysConfig) error {
	var (
		defcfg sysConfig
		err    error
	)

	if err = toml.Unmarshal([]byte(base), &defcfg); err != nil {
		return errors.Wrap(err, "default config")
	}

	if cfgfile != "" {
		b, err := ioutil.ReadFile(cfgfile)
		if err != nil {
			return errors.Wrap(err, "read config file")
		}

		if err = toml.Unmarshal(b, cfg); err != nil {
			return errors.Wrap(err, "parse config file")
		}
	}

	mergo.Merge(cfg, defcfg)

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Fprintf(os.Stdout, "%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err error
		cfg sysConfig
	)

	if err = loadConfiguration(DEFAULT, *config, &cfg); err != nil {
		log.Fatal(err)
	}

	// Initialize a pool of Redis connections
	pool := newPool(cfg.Redis.Address)

	// Connect to the Surveyor gRPC server
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(cfg.Surveyor.Address, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	client := api.NewSurveyorClient(conn)

	// Load templates
	tmpl := loadTemplates(&cfg)

	// Setup the HTTP mux
	r := mux.NewRouter()
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/",
		http.FileServer(http.Dir(cfg.Paths.Static))))
	b := sse.NewBroker()
	r.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		pr := api.PointReq{Id: "origin"}
		p, err := client.LookupPoint(context.Background(), &pr)
		w.Header().Set("Content-Type", "text/html")
		if err != nil {
			tmpl.ExecuteTemplate(w, "index.html", &api.Point{})
		} else {
			err = tmpl.ExecuteTemplate(w, "index.html", p)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		log.Printf("%q %q", req.URL, req.RemoteAddr)
	})
	r.Handle("/stream", b)
	r.HandleFunc("/ref", func(w http.ResponseWriter, req *http.Request) {
		conn := pool.Get()
		defer conn.Close()
		if data := getSurvey(conn); data != nil {
			ma := jsonpb.Marshaler{EmitDefaults: true}
			if msg, err := ma.MarshalToString(data); err == nil {
				w.Header().Set("Content-Type", "application/json")
				w.Write([]byte(msg))
			}
		} else {
			http.Error(w, "No survey found", http.StatusNotFound)
			return
		}
		log.Printf("%q %q", req.URL, req.RemoteAddr)
	})
	r.HandleFunc("/ref/xyz", func(w http.ResponseWriter, req *http.Request) {
		conn := pool.Get()
		defer conn.Close()
		if data := getSurvey(conn); data != nil {
			w.Header().Set("Content-Type", "text/plain")
			w.Header().Set("Content-Disposition",
				"attachment; filename=\"survey.xyz\"")
			tmpl.ExecuteTemplate(w, "survey.xyz", data.Points)
		} else {
			http.Error(w, "No survey found", http.StatusNotFound)
			return
		}
		log.Printf("%q %q", req.URL, req.RemoteAddr)
	})
	r.HandleFunc("/survey", func(w http.ResponseWriter, req *http.Request) {
		conn := pool.Get()
		defer conn.Close()
		doSurvey(conn, client, w, req)
		log.Printf("%q %q", req.URL, req.RemoteAddr)
	}).Methods("POST")

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	svc := &http.Server{
		Handler:     r,
		Addr:        cfg.Server.Address,
		ReadTimeout: 15 * time.Second,
	}

	psc := redis.PubSubConn{Conn: pool.Get()}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
			svc.Shutdown(context.Background())
		}
	}()

	go dataReader(psc, client, b)
	psc.Subscribe("data.refs", "data.ranges")

	log.Fatal(svc.ListenAndServe())
}
