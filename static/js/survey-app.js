/**
 * Application code for surveyapp.
 */
define(["jquery", "moment", "plot", "dmm"],
       function($, M, plot, Dmm) {

           // Time-stamp display format
           var tformat = "YYYY-MM-DD HH:mm:ss";

           // Create a survey.xyz file in a textarea
           function create_survey_file($output, hydros, scale) {
               var i, p, name, cols;
               $output.val("");
               for(i=1;i <= 8;i++) {
                   name = "h"+i;
                   p = hydros[name];
                   cols = [];
                   cols.push(" "+i);
                   cols.push(name.toUpperCase());
                   cols.push(Number(p.x/scale).toFixed(3));
                   cols.push(Number(p.y/scale).toFixed(3));
                   cols.push("100");
                   cols.push("Tracking Hyd");
                   $output.val($output.val() + cols.join(", ")+"\r\n");
               }
           }

           function check_status(data_url, chart, timeout) {
               var source = new EventSource(data_url);
               var task_id = null;
               var normal_color = $("#timestamp").css("color");
               var warning = function() {
                   var $elem = $("#timestamp");
                   $elem.css({"color": "red"});
               };

               if(timeout > 0) {
                   task_id = setTimeout(warning, timeout*1000);
               }

               source.addEventListener("gps", function(e){
                   var data = JSON.parse(e.data);
                   var lat = new Dmm(data.latitude/1.0e7);
                   var lon = new Dmm(data.longitude/1.0e7);
                   var hlat = "N", hlon = "E";

                   if(task_id) {
                       clearTimeout(task_id);
                       $("#timestamp").css({"color": normal_color});
                       task_id = setTimeout(warning, timeout*1000);
                   }

                   if(lat.isneg()) {
                       lat = lat.abs();
                       hlat = "S";
                   }

                   if(lon.isneg()) {
                       lon = lon.abs();
                       hlon = "W";
                   }

                   $("#lat").text(lat.tostring()+hlat);
                   $("#lon").text(lon.tostring()+hlon);
                   $("#timestamp").text(M(Number(data.tsec)*1000).utc().format(tformat) + "Z");

               });

               source.addEventListener("ranges", function(e) {
                   var data = JSON.parse(e.data);
                   var $elem, points;
                   $.each(data, function(key, value) {
                       var sel = "#" + key;
                       $(sel).text(Number(value).toFixed(2));
                   });
               });

               source.addEventListener("refs", function(e) {
                   var data = JSON.parse(e.data);
                   var $elem, points;
                   $.each(data.points, function(key, value) {
                       plot.update(chart, [value], 1.0e3);
                   });
               });

           }

           return {
               check_status: check_status,
               create_survey_file: create_survey_file
           };
       });
